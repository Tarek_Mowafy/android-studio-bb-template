package ${packageName};

import android.app.Application;

public class BackbaseApplication extends Application {

    private static final String configFilePath = "backbase/conf/configuration.json";


    @Override
    public void onCreate() {
        super.onCreate();
        Cxp.setLogLevel(Cxp.LogLevel.DEBUG);
        Cxp.initialize(this, configFilePath, false);
    }
}
