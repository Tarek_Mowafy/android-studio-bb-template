{
  "development": {
    "debugEnable": true
  },
  "portal": {
    "localModelPath": "$(contextRoot)/conf/model.json",
    "name": "training",
    "serverURL": "https://mobiletest.backbasecloud.com/portalserver"
  },
  "security": {
  },
  "template": {
    "extraLibraries": [
      "static/features/[BBHOST]/angular/angular.min.js",
      "static/features/[BBHOST]/fastclick/lib/fastclick.js",
      "static/features/[BBHOST]/angular-touch/angular-touch.min.js",
      "static/features/[BBHOST]/angular-i18n/angular-locale_nl-nl.js",
      "static/features/[BBHOST]/jquery/dist/jquery.min.js"
    ],
    "scripts": [
      "if (document.body) { FastClick.attach(document.body); } else { document.addEventListener('DOMContentLoaded', function() { FastClick.attach(document.body); }) }"
    ],
    "styles": [
      "static/features/[BBHOST]/shc-theme-retail-mobile/dist/styles/main.css"
    ]
  }
}
