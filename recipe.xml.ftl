<?xml version="1.0"?>
<recipe>
    <dependency mavenUrl="com.google.android.gms:play-services:4.2.42" />
    <dependency mavenUrl="com.android.support:appcompat-v7:${targetApi}.+" />
    <dependency mavenUrl="com.android.support:design:${targetApi}.+" />

    <merge from="root/AndroidManifest.xml.ftl"
             to="${escapeXmlAttribute(manifestOut)}/AndroidManifest.xml" />




    <instantiate from="root/res/layout/activity_main.xml.ftl"
                    to="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />

    <instantiate from="root/res/layout/header.xml.ftl"
                     to="${escapeXmlAttribute(resOut)}/layout/header.xml" />

    <instantiate from="root/res/layout/page_fragment.xml.ftl"
                    to="${escapeXmlAttribute(resOut)}/layout/page_fragment.xml" />

    <instantiate from="root/res/layout/splash.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/splash.xml" />

    <instantiate from="root/res/layout/vertical_layout.xml.ftl"
                  to="${escapeXmlAttribute(resOut)}/layout/vertical_layout.xml" />

    <instantiate from="root/res/layout/web_alert_dialog.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/web_alert_dialog.xml" />







     <copy from="root/res/drawable"
                 to="${escapeXmlAttribute(resOut)}/drawable" />
     <copy from="root/res/drawable-hdpi"
                 to="${escapeXmlAttribute(resOut)}/drawable-hdpi" />
     <copy from="root/res/drawable-mdpi"
                 to="${escapeXmlAttribute(resOut)}/drawable-mdpi" />
     <copy from="root/res/drawable-nodpi"
                 to="${escapeXmlAttribute(resOut)}/drawable-nodpi" />
     <copy from="root/res/drawable-xhdpi"
                 to="${escapeXmlAttribute(resOut)}/drawable-xhdpi" />
     <copy from="root/res/drawable-xxhdpi"
                 to="${escapeXmlAttribute(resOut)}/drawable-xxhdpi" />
     <copy from="root/res/drawable-xxxhdpi"
                 to="${escapeXmlAttribute(resOut)}/drawable-xxxhdpi" />




   <merge from="root/res/values/colors.xml.ftl"
            to="${escapeXmlAttribute(resOut)}/values/colors.xml" />

   <merge from="root/res/values/dimens.xml.ftl"
           to="${escapeXmlAttribute(resOut)}/values/dimens.xml" />

   <merge from="root/res/values/styles.xml.ftl"
           to="${escapeXmlAttribute(resOut)}/values/styles.xml" />

    <merge from="root/res/values/strings.xml.ftl"
             to="${escapeXmlAttribute(resOut)}/values/strings.xml" />



    <instantiate from="root/src/app_package/MainActivity.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/MainActivity.java" />




   <merge from="root/build.gradle"
                      to="${escapeXmlAttribute(projectOut)}/build.gradle" />



    <instantiate from="root/src/app_package/BackbaseApplication.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/BackbaseApplication.java" />

    <instantiate from="root/src/app_package/MainActivity.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/MainActivity.java" />

    <instantiate from="root/src/app_package/ContactPlugin.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/ContactPlugin.java" />

    <instantiate from="root/src/app_package/PageFragment.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/PageFragment.java" />

   <instantiate from="root/assets.gradle.ftl"
                  to="${escapeXmlAttribute(projectOut)}/assets.gradle" />




   <instantiate from="root/assets/backbase/conf/configuration.json.ftl"
                                   to="${escapeXmlAttribute(assetsOut)}/conf/configuration.json" />


   <instantiate from="root/assets/backbase/conf/model.json.ftl"
                  to="${escapeXmlAttribute(assetsOut)}/conf/model.json" />










</recipe>
